package sheridan;

public class Celsius {

	public static int convertFromFarenheit(int f) {
		
		double celsius = (f - 32) * (5.0/9);
		int c = (int)Math.round(Math.ceil(celsius));
		return c;
	}
	
	public static void main(String[] args) {
		int celsius = convertFromFarenheit(0);
		System.out.println("Celsius is: " + celsius);
	}
}
