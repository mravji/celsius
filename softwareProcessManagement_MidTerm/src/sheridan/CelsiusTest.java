package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelsiusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testconvertFromFarenheit() {
		assertTrue("The rounding was performed correctly", Celsius.convertFromFarenheit(32) == 0);
	}

	@Test
	public void testconvertFromFarenheit_BoundaryIn() {
		assertTrue("The rounding was performed correctly", Celsius.convertFromFarenheit(5) == -15);
	}
	
	@Test
	public void testconvertFromFarenheit_BoundaryOut() {
		assertFalse("The rounding was not performed correctly", Celsius.convertFromFarenheit(6) == -14.4444);
	}
	
	@Test
	public void testconvertFromFarenheit_Exceptional() {
		assertFalse("The rounding was not performed correctly", Celsius.convertFromFarenheit(57) == 13.8889);
	}
}
